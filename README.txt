

    ____  _____         ______ ____   ____ 
   / __ \|__  /        / ____// __ \ / __ \
  / / / / /_ < ______ / /_   / /_/ // / / /
 / /_/ /___/ //_____// __/  / ____// /_/ / 
/_____//____/       /_/    /_/    /_____/  
                                           

                                                         
________________________________________________________________


This is a modified version of dhewm3 engine. You can find all needed info about compilation, etc. at the following links:

dhewm3 Original Readme - https://github.com/dhewm/dhewm3/blob/master/README.md

Compatible and tested libs for MinGW and MSVC compilers are included in the libs folder. 

Thank you for downloading D3-FPD!

Logo generated with patorjk.com ( http://patorjk.com/software/taag/#p=display&h=1&v=1&f=Slant&t=D3-FPD )
